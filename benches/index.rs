use criterion::{
    criterion_group, criterion_main, AxisScale, Criterion, ParameterizedBenchmark,
    PlotConfiguration,
};
use rand::prelude::*;
use std::time::Duration;

use ia_gen::index::Index;

#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

fn bench_indexes_usize(c: &mut Criterion) {
    for &bound in &[5, 10, 20, 50, 100, 500, 1000, 5000, 100_000, 500_000] {
        let plot_config = PlotConfiguration::default().summary_scale(AxisScale::Logarithmic);

        let mut sizes = vec![
            1, 2, 3, 4, 5, 10, 20, 30, 40, 50, 100, 200, 300, 500, 1000, 2000, 5000,
        ];
        sizes.retain(|&x| x <= bound);

        let bench = ParameterizedBenchmark::new(
            "inline",
            move |b, &amount| {
                let mut rng = StdRng::from_rng(thread_rng()).unwrap();
                b.iter(|| usize::sample_indexes_inline(&mut rng, bound, amount))
            },
            sizes,
        )
        .with_function("rejection", move |b, &amount| {
            let mut rng = StdRng::from_rng(thread_rng()).unwrap();
            b.iter(|| usize::sample_indexes_rejection(&mut rng, bound, amount))
        })
        .with_function("floyd_inline", move |b, &amount| {
            let mut rng = StdRng::from_rng(thread_rng()).unwrap();
            b.iter(|| usize::sample_indexes_floyd_inline(&mut rng, bound, amount))
        })
        .with_function("floyd_map", move |b, &amount| {
            let mut rng = StdRng::from_rng(thread_rng()).unwrap();
            b.iter(|| usize::sample_indexes_floyd_map(&mut rng, bound, amount))
        })
        .plot_config(plot_config)
        .warm_up_time(Duration::from_secs(1))
        .measurement_time(Duration::from_secs(2));
        c.bench(&format!("Index on {}", bound), bench);
    }
}

criterion_group!(benches, bench_indexes_usize);
criterion_main!(benches);
