# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- weighted! macro for convenient weighted sampling.
- Hashbrown as optional (default active) dependency, greatly speeding up 
  both range and composition sampling.
- (Unbounded) Composition sampling of both numbers and containers.
- Range sampling of multiple (distinct) numbers.
