use ia_gen;
use rand::{rngs::StdRng, SeedableRng};

#[test]
fn weighted_macro_works() {
    let index = ia_gen::weighted!('a' : 7, 'b': 0, 'c': 0);
    let mut rng = StdRng::seed_from_u64(0);

    assert_eq!('a', index[&mut rng]);
}
