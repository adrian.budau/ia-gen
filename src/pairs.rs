use rand::Rng;

pub trait SamplePairs {
    type Output;

    fn sample_pairs<R: Rng>(self, rng: &mut R) -> (Vec<Self::Output>, Vec<Self::Output>);
}

impl<T, I: IntoIterator<Item = T>> SamplePairs for I
where
    I::IntoIter: ExactSizeIterator,
{
    type Output = T;

    fn sample_pairs<R: Rng>(self, rng: &mut R) -> (Vec<T>, Vec<T>) {
        let mut iter = self.into_iter();

        let len = iter.len();
        assert_eq!(
            len % 2,
            0,
            "`SamplePairs::sample_pairs` requires an even number of elements"
        );

        let mut open = 0;
        let mut left = len;

        let mut first = Vec::with_capacity(len / 2);
        let mut second = Vec::with_capacity(len / 2);

        while left > 0 {
            let next = match iter.next() {
                Some(value) => value,
                None => break,
            };

            // probability it should go into second is
            // open * (left + open + 2) / (2 * left * (open + 1))
            let probability = rng.gen_range(0..2 * left * (open + 1));
            if probability < open * (left + open + 2) {
                second.push(next);
                open -= 1;
            } else {
                first.push(next);
                open += 1;
            }
            left -= 1;
        }
        (first, second)
    }
}
