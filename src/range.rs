use std::ops::{Range, RangeInclusive};

use rand::Rng;

use crate::index::Index;

pub trait RangeSampler {
    type Output;
    fn range_sample<R: Rng>(self, rng: &mut R, amount: usize) -> Vec<Self::Output>;
}

macro_rules! impl_range_sampler {
    ($ty:ty) => {
        impl RangeSampler for RangeInclusive<$ty> {
            type Output = $ty;

            #[inline]
            fn range_sample<R: Rng>(self, rng: &mut R, amount: usize) -> Vec<$ty> {
                if amount == 0 {
                    return Vec::new();
                }

                assert!(
                    self.start() <= self.end(),
                    "`RangeSampler::range_sample` requires non empty range when `amount > 0`"
                );

                let (start, end) = self.into_inner();
                let bound = end.wrapping_sub(start) as <$ty as Index>::Unsigned;

                // bound + 1 >= amount
                assert!(
                    bound >= (amount - 1) as <$ty as Index>::Unsigned,
                    "`RangeSampler::range_sample` requires `start..=end` to contain at least `amount` elements"
                );

                let mut indexes = <$ty as Index>::sample_indexes(rng, bound, amount);
                for index in &mut indexes {
                    *index = index.wrapping_add(start);
                }
                indexes
            }
        }

        impl RangeSampler for Range<$ty> {
            type Output = $ty;
            #[inline]
            fn range_sample<R: Rng>(self, rng: &mut R, amount: usize) -> Vec<$ty> {
                if amount == 0 {
                    return Vec::new();
                }

                assert!(
                    self.start < self.end,
                    "`RangeSampler::range_sample` requires non empty range when `amount > 0`"
                );

                let bound = self.end.wrapping_sub(self.start) as <$ty as Index>::Unsigned;
                assert!(
                    bound >= amount as <$ty as Index>::Unsigned,
                    "`RangeSampler::range_sample` requires `start..end` to contain at least `amount` elementes"
                );

                let mut indexes =
                    <$ty as Index>::sample_indexes(rng, bound.wrapping_sub(1), amount);
                for index in &mut indexes {
                    *index = index.wrapping_add(self.start);
                }
                indexes
            }
        }
    };
}

impl_range_sampler!(u8);
impl_range_sampler!(i8);
impl_range_sampler!(u16);
impl_range_sampler!(i16);
impl_range_sampler!(u32);
impl_range_sampler!(i32);
impl_range_sampler!(u64);
impl_range_sampler!(i64);
impl_range_sampler!(u128);
impl_range_sampler!(i128);
impl_range_sampler!(usize);
impl_range_sampler!(isize);

#[cfg(test)]
mod tests {
    use rand::rngs::StdRng;
    use rand::SeedableRng;

    use super::RangeSampler;

    #[test]
    fn range_sample_works() {
        let mut rng = StdRng::seed_from_u64(0);

        assert!((0..10).range_sample(&mut rng, 0).is_empty());
        assert_eq!(
            4,
            (0..10).range_sample(&mut rng, 4).len(),
            "sampling 4 out of `0..10`"
        );
        assert_eq!(
            10,
            (1..=10).range_sample(&mut rng, 10).len(),
            "sampling 10 out of `1..=10`"
        );
        assert!(
            (35..45)
                .range_sample(&mut rng, 3)
                .into_iter()
                .all(|x| x >= 35 && x <= 45),
            "sampling 3 out of `35..45` samples inside `35..45`"
        );
    }

    #[test]
    fn range_negative_only() {
        let mut rng = StdRng::seed_from_u64(0);

        let generated = (-10..-5).range_sample(&mut rng, 3);
        assert_eq!(3, generated.len(), "sampling 3 out of `-10..-5`");
        assert!(generated.into_iter().all(|x| x >= -10 && x < 5));
    }

    #[test]
    fn range_sample_usize_limit() {
        let mut rng = StdRng::seed_from_u64(0);

        assert_eq!(
            3,
            (usize::min_value()..=usize::max_value())
                .range_sample(&mut rng, 3)
                .len(),
            "sampling 3 out of `usize::MIN..=usize::MAX`"
        );
    }

    #[test]
    fn range_large_numbers() {
        let mut rng = StdRng::seed_from_u64(0);

        assert_eq!(
            3,
            (2u128..0x00ff_ffff_ffff_ffff_ffffu128)
                .range_sample(&mut rng, 3)
                .len(),
            "sampling 3 out of a large range"
        );
        assert_eq!(
            5,
            (u128::min_value()..=u128::max_value())
                .range_sample(&mut rng, 5)
                .len(),
            "sampling 5 out of `u128::MIN..=u128::MAX`"
        );
        assert_eq!(
            5,
            (i128::min_value()..=i128::max_value())
                .range_sample(&mut rng, 5)
                .len(),
            "sampling 5 out of `i128::MIN..=i128::MAX`"
        );
    }
}
