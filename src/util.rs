use std::ops::{AddAssign, Index, IndexMut};

use rand::distributions::{
    uniform::{SampleBorrow, SampleUniform},
    Distribution, WeightedError, WeightedIndex,
};
use rand::Rng;

pub struct WeightedChoice<T, X: SampleUniform + PartialOrd> {
    values: Vec<T>,
    index: WeightedIndex<X>,
}

impl<T, X: SampleUniform + PartialOrd> WeightedChoice<T, X> {
    pub fn new<I, R>(weights: I) -> Result<Self, WeightedError>
    where
        I: IntoIterator<Item = (T, R)>,
        R: SampleBorrow<X>,
        X: for<'a> AddAssign<&'a X> + Clone + Default,
    {
        let mut values = Vec::new();
        let index = WeightedIndex::new(weights.into_iter().map(|(value, weight)| {
            values.push(value);
            weight
        }))?;
        Ok(Self { values, index })
    }
}

impl<T, X: SampleUniform + PartialOrd, R: Rng> Index<&mut R> for WeightedChoice<T, X> {
    type Output = T;

    fn index(&self, rng: &mut R) -> &T {
        let index = self.index.sample(rng);
        &self.values[index]
    }
}

impl<T, X: SampleUniform + PartialOrd, R: Rng> IndexMut<&mut R> for WeightedChoice<T, X> {
    fn index_mut(&mut self, rng: &mut R) -> &mut T {
        let index = self.index.sample(rng);
        &mut self.values[index]
    }
}

#[macro_export]
#[doc(hidden)]
macro_rules! weighted_unexpected {
    () => {};
}

// tt muncher
#[macro_export]
macro_rules! weighted {
    (@vec () () ($((($($key:tt)*), $value:expr))+)) => {
        vec![$(($($key)*,$value)),+]
    };

    (@vec () (($key:expr) : $value:expr) ($($pairs:tt)*)) => {
        $crate::weighted!(@vec () () ($($pairs)* (($key), $value)))
    };

    (@vec () (($key:expr) : $value:expr , $($tt:tt)*) ($($pairs:tt)*)) => {
        $crate::weighted!(@vec () ($($tt)*) ($($pairs)* (($key), $value)))
    };

    (@vec ($($key:tt)*) (: $value:expr) ($($pairs:tt)*)) => {
        $crate::weighted!(@vec () () ($($pairs)* (($($key)*), $value)))
    };

    (@vec ($($key:tt)*) (: $value:expr , $($tt:tt)*) ($($pairs:tt)*)) => {
        $crate::weighted!(@vec () ($($tt)*) ($($pairs)* (($($key)*), $value)))
    };

    (@vec ($($key:tt)*) (: $($tt:tt)*) ($($pairs:tt)*)) => {
        $crate::weighted_unexpected!($($tt)*)
    };

    (@vec ($($key:tt)*) ($key_part:tt $($tt:tt)*) ($($pairs:tt)*)) => {
        $crate::weighted!(@vec ($($key)* $key_part) ($($tt)*) ($($pairs)*))
    };

    (@vec ($($key:tt)*) ($($tt:tt)*) ($($pairs:tt)*)) => {
        $crate::weighted_unexpected!($($tt)*)
    };

    ($($tt:tt)+) => {
        $crate::util::WeightedChoice::new($crate::weighted!(@vec () ($($tt)+) ())).unwrap()
    };
}
