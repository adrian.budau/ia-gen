#[cfg(not(feature = "hashbrown"))]
use std::collections::HashSet;
use std::iter::FromIterator;

#[cfg(feature = "hashbrown")]
use hashbrown::HashSet;
use rand::distributions::{Distribution, Uniform};
use rand::Rng;

pub trait Index: Sized {
    type Unsigned;

    fn sample_indexes_inline<R: Rng>(
        rng: &mut R,
        bound: Self::Unsigned,
        amount: usize,
    ) -> Vec<Self>;
    fn sample_indexes_rejection<R: Rng>(
        rng: &mut R,
        bound: Self::Unsigned,
        amount: usize,
    ) -> Vec<Self>;
    fn sample_indexes_floyd_inline<R: Rng>(
        rng: &mut R,
        bound: Self::Unsigned,
        amount: usize,
    ) -> Vec<Self>;
    fn sample_indexes_floyd_map<R: Rng>(
        rng: &mut R,
        bound: Self::Unsigned,
        amount: usize,
    ) -> Vec<Self>;
    fn sample_indexes<R: Rng>(rng: &mut R, bound: Self::Unsigned, amount: usize) -> Vec<Self>;
}

macro_rules! impl_index {
    ($ty:ty, $unsigned:ty) => {
        impl Index for $ty {
            type Unsigned = $unsigned;

            #[inline]
            fn sample_indexes_rejection<R: Rng>(
                rng: &mut R,
                bound: $unsigned,
                amount: usize,
            ) -> Vec<Self> {
                let mut indices = Vec::with_capacity(amount);
                let mut cache = HashSet::with_capacity(amount);
                let distribution = Uniform::new_inclusive(0 as $unsigned, bound);
                for _ in 0..amount {
                    loop {
                        let pos = distribution.sample(rng);
                        if cache.insert(pos) {
                            indices.push(pos as Self);
                            break;
                        }
                    }
                }
                indices
            }

            #[inline]
            fn sample_indexes_floyd_inline<R: Rng>(
                rng: &mut R,
                bound: $unsigned,
                amount: usize,
            ) -> Vec<Self> {
                let mut indices = Vec::with_capacity(amount);

                let start = bound.wrapping_sub(amount as $unsigned).wrapping_add(1);

                for i in start..=bound {
                    let distribution = Uniform::new_inclusive(0 as $unsigned, i);
                    let pos = distribution.sample(rng) as Self;
                    if indices.contains(&pos) {
                        indices.push(i as Self)
                    } else {
                        indices.push(pos);
                    }
                }
                for i in (1..amount).rev() {
                    indices.swap(i, rng.gen_range(0..i + 1));
                }
                indices
            }

            #[inline]
            fn sample_indexes_floyd_map<R: Rng>(
                rng: &mut R,
                bound: $unsigned,
                amount: usize,
            ) -> Vec<Self> {
                let mut indices = Vec::with_capacity(amount);
                let mut cache = HashSet::with_capacity(amount);

                let start = bound.wrapping_sub(amount as $unsigned).wrapping_add(1);

                for i in start..=bound {
                    let distribution = Uniform::new_inclusive(0 as $unsigned, i);
                    let pos = distribution.sample(rng);
                    if cache.insert(pos) {
                        indices.push(pos as Self)
                    } else {
                        cache.insert(i);
                        indices.push(i as Self);
                    }
                }
                indices
            }

            #[inline]
            fn sample_indexes_inline<R: Rng>(
                rng: &mut R,
                bound: $unsigned,
                amount: usize,
            ) -> Vec<Self> {
                let mut indices = Vec::from_iter((0 as $unsigned..=bound).map(|x| x as Self));
                for i in 0..amount {
                    let distribution = Uniform::new_inclusive(i, bound as usize);
                    let j = distribution.sample(rng);
                    indices.swap(i, j as usize);
                }
                indices.truncate(amount);
                indices
            }

            #[inline]
            fn sample_indexes<R: Rng>(rng: &mut R, bound: $unsigned, amount: usize) -> Vec<Self> {
                // Hashbrown is pretty damn fast
                if bound / 5 > amount as $unsigned {
                    if amount < 4 {
                        Self::sample_indexes_floyd_inline(rng, bound, amount)
                    } else {
                        Self::sample_indexes_rejection(rng, bound, amount)
                    }
                } else {
                    Self::sample_indexes_inline(rng, bound, amount)
                }
            }
        }
    };

    ($ty:ty) => {
        impl_index!($ty, $ty);
    };
}

impl_index!(u8);
impl_index!(i8, u8);
impl_index!(u16);
impl_index!(i16, u16);
impl_index!(u32);
impl_index!(i32, u32);
impl_index!(u64);
impl_index!(i64, u64);
impl_index!(u128);
impl_index!(i128, u128);
impl_index!(usize);
impl_index!(isize, usize);
