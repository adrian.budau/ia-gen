use std::ops::{Range, RangeFrom, RangeFull, RangeInclusive, RangeTo, RangeToInclusive};

use rand::distributions::{Distribution, Uniform};
use rand::Rng;

use crate::range::RangeSampler;

use self::hidden::FromToIterator;

mod hidden {
    use std::collections::{
        BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, LinkedList, VecDeque,
    };
    use std::hash::Hash;
    use std::iter::FromIterator;

    pub trait FromToIterator: IntoIterator + FromIterator<<Self as IntoIterator>::Item> {}

    impl<T> FromToIterator for Vec<T> {}
    impl<T> FromToIterator for VecDeque<T> {}
    impl<T> FromToIterator for LinkedList<T> {}
    impl<Key: Hash + Eq, Value> FromToIterator for HashMap<Key, Value> {}
    impl<Key: Ord, Value> FromToIterator for BTreeMap<Key, Value> {}
    impl<T: Hash + Eq> FromToIterator for HashSet<T> {}
    impl<T: Ord> FromToIterator for BTreeSet<T> {}
    impl<T: Ord> FromToIterator for BinaryHeap<T> {}

    #[cfg(feature = "hashbrown")]
    impl<Key: Hash + Eq, Value> FromToIterator for hashbrown::HashMap<Key, Value> {}
    #[cfg(feature = "hashbrown")]
    impl<T: Hash + Eq> FromToIterator for hashbrown::HashSet<T> {}
}

pub trait SampleComposition {
    type Output;

    fn sample_composition<R: Rng>(self, rng: &mut R, parts: usize) -> Vec<Self::Output>;
}

pub trait SampleBoundedComposition<B> {
    type Output;

    fn sample_bounded_composition<R: Rng>(
        self,
        rng: &mut R,
        parts: usize,
        bounds: B,
    ) -> Vec<Self::Output>;
}

macro_rules! impl_composition {
    ($ty:ty) => {
        impl SampleComposition for $ty {
            type Output = Self;

            #[inline]
            fn sample_composition<R: Rng>(self, rng: &mut R, parts: usize) -> Vec<Self> {
                if parts == 0 {
                    assert!(
                        self == 0,
                        "`SampleComposition::sample_composition` requires `self == 0` when `parts is 0`"
                    );
                    return Vec::new();
                }

                self.sample_bounded_composition(rng, parts, 0..)
            }
        }

        impl SampleBoundedComposition<RangeFrom<$ty>> for $ty {
            type Output = Self;

            #[inline]
            fn sample_bounded_composition<R: Rng>(self, rng: &mut R, parts: usize, bound: RangeFrom<$ty>) -> Vec<Self> {
                if parts == 0 {
                    assert!(
                        self == 0,
                        "`SampleBoundedComposition::sample_bounded_composition` requires `self == 0` when `parts is 0`"
                    );
                    return Vec::new();
                }

                let start = bound.start.checked_mul(parts as $ty).expect("`SampleBoundedComposition::sample_bounded_composition` requires `bound.start * parts` too fit in data type");

                assert!(
                    self >= start,
                    "`SampleBoundeComposition::sample_bounded_composition` requires `bound.start * parts <= self`"
                );
                assert!(
                    self.checked_sub(start - bound.start).is_some(),
                    "`SampleBoundComposition::sample_bounded_composition` might end up sampling numbers outside data type limits"
                );
                let (delta_start, mut splits, end) = if bound.start > 0 || Self::min_value() != 0 {
                    let delta_start = start.checked_sub(parts as $ty).expect("`SampleBoundedComposition::sample_bounded_composition` invariant broken. Bound and/or start too big for data type.");
                    let splits = (delta_start + 1..self).range_sample(rng, parts - 1);
                    (delta_start, splits, self)
                } else {
                    let end = self.checked_add(parts as $ty).expect("`SampleBoundedComposition::sample_bounded_composition` invariant broken. Bound and/or end too big for data type.");
                    let splits = (start + 1..end).range_sample(rng, parts - 1);
                    (start, splits, end)
                };

                splits.sort();
                let mut parts = Vec::with_capacity(parts);

                let delta = bound.start.wrapping_sub(1);
                let mut last = delta_start;
                for split in splits {
                    parts.push(split.wrapping_sub(last).wrapping_add(delta));
                    last = split;
                }
                parts.push(end.wrapping_sub(last).wrapping_add(delta));
                parts
            }
        }

        impl SampleBoundedComposition<RangeTo<$ty>> for $ty {
            type Output = Self;

            #[inline]
            fn sample_bounded_composition<R: Rng>(self, rng: &mut R, parts: usize, bound: RangeTo<$ty>) -> Vec<Self> {
                return self.sample_bounded_composition(rng, parts, 0..bound.end);
            }
        }

        impl SampleBoundedComposition<Range<$ty>> for $ty {
            type Output = Self;

            #[allow(clippy::range_minus_one)]
            #[inline]
            fn sample_bounded_composition<R: Rng>(self, rng: &mut R, parts: usize, bound: Range<$ty>) -> Vec<Self> {
                if parts != 0 {
                    assert!(bound.start < bound.end,
                        "`SampleBoundedComposition::sample_bounded_composition` requires a proper bound"
                    );
                }
                return self.sample_bounded_composition(rng, parts, bound.start..=(bound.end - 1));
            }
        }

        impl SampleBoundedComposition<RangeToInclusive<$ty>> for $ty {
            type Output = Self;

            #[inline]
            fn sample_bounded_composition<R: Rng>(self, rng: &mut R, parts: usize, bound: RangeToInclusive<$ty>) -> Vec<Self> {
                return self.sample_bounded_composition(rng, parts, 0..=bound.end);
            }
        }


        impl SampleBoundedComposition<RangeInclusive<$ty>> for $ty {
            type Output = Self;

            #[inline]
            fn sample_bounded_composition<R: Rng>(self, rng: &mut R, parts: usize, bound: RangeInclusive<$ty>) -> Vec<Self> {
                let (start, end) = bound.into_inner();
                loop {
                    let sample = self.sample_bounded_composition(rng, parts, start..);

                    if sample.iter().all(|&x| x <= end) {
                        return sample;
                    }
                }
            }
        }

    };
}

impl_composition!(u8);
impl_composition!(i8);
impl_composition!(u16);
impl_composition!(i16);
impl_composition!(u32);
impl_composition!(i32);
impl_composition!(u64);
impl_composition!(i64);
impl_composition!(u128);
impl_composition!(i128);
impl_composition!(usize);
impl_composition!(isize);

impl<T: FromToIterator> SampleComposition for T {
    type Output = T;

    fn sample_composition<R: Rng>(self, rng: &mut R, parts: usize) -> Vec<Self> {
        if parts == 0 {
            assert!(
                self.into_iter().count() == 0,
                "`SampleComposition::sample_composition` requires `self.len == 0` when `parts is 0`"
            );
            return Vec::new();
        }

        let mut split = Vec::with_capacity(parts);
        for _ in 0..parts {
            split.push(Vec::new());
        }

        let distribution = Uniform::new(0, parts);
        for element in self.into_iter() {
            split[distribution.sample(rng)].push(element);
        }

        split.into_iter().map(|x| x.into_iter().collect()).collect()
    }
}

impl<T: SampleComposition> SampleBoundedComposition<RangeFull> for T {
    type Output = T::Output;

    fn sample_bounded_composition<R: Rng>(
        self,
        rng: &mut R,
        parts: usize,
        _: RangeFull,
    ) -> Vec<Self::Output> {
        self.sample_composition(rng, parts)
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use rand::rngs::StdRng;
    use rand::SeedableRng;

    use super::{SampleBoundedComposition, SampleComposition};

    #[test]
    fn sample_composition_works() {
        let mut rng = StdRng::seed_from_u64(0);

        let elements = 10.sample_composition(&mut rng, 3);
        assert_eq!(3, elements.len(), "a composition of 10 into 3 parts");
        assert!(
            elements.iter().all(|&x| x <= 10),
            "a composition of 10 into 3 parts contains only numbers smaller than 10"
        );
        assert_eq!(
            10,
            elements.into_iter().sum(),
            "a composition of 10 into 3 parts sum"
        );
    }

    #[test]
    fn sample_composition_big_numbers() {
        let mut rng = StdRng::seed_from_u64(0);

        // there is no way to sample unsined::max_value into values >=0
        // at some point we'd need a value > unsigned:max_value
        let elements = usize::max_value().sample_bounded_composition(&mut rng, 7, 1..);
        assert_eq!(
            7,
            elements.len(),
            "a composition of `usize::max_value()` into 7 parts"
        );
        assert!(
            elements.iter().all(|&x| x >= 1),
            "a composition of `usize::max_value()` has only positive numbers"
        );

        assert_eq!(
            usize::max_value(),
            elements.into_iter().sum(),
            "a composition of `usize::max_value()` into 7 parts sum"
        );

        let elements = u128::max_value().sample_bounded_composition(&mut rng, 7, 1..);
        assert_eq!(
            7,
            elements.len(),
            "a composition of `u128::max_value()` into 7 parts"
        );
        assert!(
            elements.iter().all(|&x| x >= 1),
            "a composition of `u128::max_value()` has only positive numbers"
        );
        assert_eq!(
            u128::max_value(),
            elements.into_iter().sum(),
            "a composition of `u128::max_value()` into 7 parts sum"
        );

        let elements = i128::max_value().sample_composition(&mut rng, 7);
        assert_eq!(
            7,
            elements.len(),
            "a composition of `i128::max_value()` into 7 parts"
        );
        assert!(
            elements.iter().all(|&x| x >= 10),
            "a composition of `i128::max_value()` has only nonnegative numbers"
        );

        assert_eq!(
            i128::max_value(),
            elements.into_iter().sum(),
            "a composition of `i128::max_value()` into 7 parts sum"
        );
    }

    #[test]
    fn sample_composition_iter() {
        let mut rng = StdRng::seed_from_u64(0);

        let hash: HashSet<_> = (0..12).collect();

        let parts = hash.clone().sample_composition(&mut rng, 4);
        assert_eq!(4, parts.len(), "a composition of `0..12` into 4 parts");
        assert_eq!(
            12,
            parts.iter().map(HashSet::len).sum::<usize>(),
            "sum of each part in a composition of `0..12`"
        );
        assert_eq!(
            hash,
            parts.into_iter().flatten().collect(),
            "adding up the composition of `0..12` into 4 parts"
        );
    }

    #[test]
    fn sampling_negative_compositions() {
        let mut rng = StdRng::seed_from_u64(0);

        let elements = (-15).sample_bounded_composition(&mut rng, 3, (-5)..);

        assert_eq!(3, elements.len(), "a composition of -15 into 3 parts >= -5");
        assert!(
            elements.iter().all(|&x| x >= -5),
            "a composition of -15 into 3 parts has only values >= -5"
        );
        assert_eq!(
            -15,
            elements.into_iter().sum(),
            "a composition of -15 into 3 parts sum"
        );
    }

    #[test]
    fn sampling_whole_range() {
        let mut rng = StdRng::seed_from_u64(0);

        assert_eq!(
            u8::max_value() as usize,
            u8::max_value()
                .sample_bounded_composition(&mut rng, u8::max_value() as usize, 1..)
                .len(),
            "a composition of `u8::max_value()` into `u8::max_value()` parts >= 1"
        );
    }

    #[test]
    fn sampling_zero() {
        let mut rng = StdRng::seed_from_u64(0);

        assert_eq!(
            vec![0; 1],
            0usize.sample_bounded_composition(&mut rng, 1, 0..)
        );
        assert_eq!(
            vec![0; 5],
            0usize.sample_bounded_composition(&mut rng, 5, 0..)
        );
        assert_eq!(vec![0; 7], 0usize.sample_composition(&mut rng, 7));
    }

    #[test]
    fn sampling_unsigned() {
        let mut rng = StdRng::seed_from_u64(0);

        assert_eq!(
            15usize,
            15usize.sample_composition(&mut rng, 7).into_iter().sum()
        );
    }
}
